<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>AnyCar - HTML Template for Automotive &amp; Business</title>

    <?php
        include "head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <?php
        include "preloader.php";
    ?>
    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
        <?php
            include "header.php";
        ?>
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <!-- Map -->
            <section class="flat-row pad-top0px">                
                <div id="flat-map"></div>                     
            </section>

            <section class="flat-row pad-top0px pad-bottom0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <form  id="contactform" class="flat-contact-form" method="post" action="./contact/contact-process.php">
                                <div class="quick-appoinment">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" id="name" name="name" class="input-text-name" placeholder="Name" required="required">
                                        </div><!-- /.col-md-6 -->
                                        <div class="col-md-6">
                                            <input type="text" id="email" name="email" class="input-text-email" placeholder="Email" required="required">
                                        </div><!-- /.col-md-6 -->
                                    </div><!-- /.row -->

                                    <div class="flat-divider d30px"></div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" id="phone" name="phone" class="input-text-phone" placeholder="Phone" required="required">
                                        </div><!-- /.col-md-6 -->
                                        <div class="col-md-6">
                                            <input type="text" id="subject" name="subject" class="input-text-subject" placeholder="Subject" required="required">
                                        </div><!-- /.col-md-6 -->
                                    </div><!-- /.row -->

                                    <div class="flat-divider d30px"></div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <textarea class="textarea-question" id="message" name="message" placeholder="Message" required="required"></textarea>
                                        </div><!-- /.col-md-12 -->
                                    </div><!-- /.row -->

                                    <div class="flat-divider d26px"></div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="submit-wrap">
                                                <button class="flat-button bg-theme">Send Your Message</button>
                                            </div>
                                        </div><!-- /.col-md-12 -->
                                    </div><!-- /.row -->
                                </div>
                            </form>
                        </div><!-- /.col-md-8 -->                        
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->
        
            <div class="flat-divider d50px"></div>
        </div><!--/.site-content -->

        <?php
            include "footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIm1AxfRgiI_w36PonGqb_uNNMsVGndKo&v=3.7"></script>
    <script type="text/javascript" src="javascript/gmap3.min.js"></script>
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/switcher.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>