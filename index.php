<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Alexa Taxician Point - Repair Services</title>

    <?php
        include "head.php";
    ?>

</head>                                 
<body class="header-sticky">
    <?php
        include "preloader.php";
    ?>

    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            
            <?php
                include "header.php";
            ?>
            <!-- Slider -->
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>
                        <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="<?=$root_path?>images/slides/demo.jpg" alt="slider-image" />
                            <div class="tp-caption sfl title-slide center" data-x="center" data-y="80" data-hoffset="0" data-voffset="-20" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                                Repairs and Installations
                            </div>  
                            <div class="tp-caption sfr title-slide1 center" data-x="center" data-y="130" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                                Avail features from Alexa Taxician Point
                            </div>    
                            <div class="tp-caption sfl flat-button-slider" data-x="415" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Read More</a>
                            </div>

                            <div class="tp-caption sfl flat-and" data-x="565" data-y="257" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <span>&amp;</span>
                            </div>

                            <div class="tp-caption sfr flat-button-slider style1" data-x="615" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Click Here</a>
                            </div>                   
                        </li>

                        <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="<?=$root_path?>images/slides/slider_2.jpg" alt="slider-image" />
                            <div class="tp-caption sfl title-slide center" data-x="center" data-y="80" data-hoffset="0" data-voffset="-20" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                                NEW ELECTRONICS REPAIRING
                            </div>  
                            <div class="tp-caption sfr title-slide1 center" data-x="center" data-y="130" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                                High Quality Service and Low Prices
                            </div>    
                            <div class="tp-caption sfl flat-button-slider" data-x="415" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Read More</a>
                            </div>

                            <div class="tp-caption sfl flat-and" data-x="565" data-y="257" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <span>&amp;</span>
                            </div>

                            <div class="tp-caption sfr flat-button-slider style1" data-x="615" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Click Here</a>
                            </div>                   
                        </li>

                        <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="<?=$root_path?>images/slides/slider_3.jpg" alt="slider-image" />
                            <div class="tp-caption sfl title-slide center" data-x="center" data-y="80" data-hoffset="0" data-voffset="-20" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                                Nationwide Warranty
                            </div>  
                            <div class="tp-caption sfr title-slide1 center" data-x="center" data-y="130" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                                Our warranty rights are protected
                            </div>    
                            <div class="tp-caption sfl flat-button-slider" data-x="415" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Read More</a>
                            </div>

                            <div class="tp-caption sfl flat-and" data-x="565" data-y="257" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <span>&amp;</span>
                            </div>

                            <div class="tp-caption sfr flat-button-slider style1" data-x="615" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Click Here</a>
                            </div>                   
                        </li>

                        <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="<?=$root_path?>images/slides/slider_4.jpg" alt="slider-image" />
                            <div class="tp-caption sfl title-slide center" data-x="center" data-y="80" data-hoffset="0" data-voffset="-20" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">
                                Air Conditioning Service
                            </div>  
                            <div class="tp-caption sfr title-slide1 center" data-x="center" data-y="130" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">
                                From repairing to installation
                            </div>    
                            <div class="tp-caption sfl flat-button-slider" data-x="415" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Read More</a>
                            </div>

                            <div class="tp-caption sfl flat-and" data-x="565" data-y="257" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <span>&amp;</span>
                            </div>

                            <div class="tp-caption sfr flat-button-slider style1" data-x="615" data-y="250" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                                <a href="<?=$root_path?>about-us" class="button-slider">Click Here</a>
                            </div>                   
                        </li>
                    </ul>
                </div>
            </div>
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <section class="client">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>THE BRANDS</h4>
                        </div>
                        <div class="col-md-8">
                            <div class="wrap-img-client">
                                <img src="<?=$root_path?>images/client/1.png" alt="images">
                            </div>

                            <div class="wrap-img-client">
                                <img src="<?=$root_path?>images/client/2.png" alt="images">
                            </div>

                            <div class="wrap-img-client">
                                <img src="<?=$root_path?>images/client/3.png" alt="images">
                            </div>

                            <div class="wrap-img-client">
                                <img src="<?=$root_path?>images/client/4.png" alt="images">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Services -->
            <section class="flat-row services pad-bottom0px">
                <div class="container">
                    <h1 class="text-center" style="font-weight: bold;">WELCOME TO <span style="color: #f22613;">Alexa Taknicen Point</span></h1>
                    <p class="text-center" style="font-size: 1.4em;">Our experts provide home appliances repair services in your area at your given time. On-time support for any kind of electronic item Geyser, Fridge, Washing machine, Microwave Problems.</p>
                    <div class="row">
                       <div class="three-columns">
                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-inspection.html">
                                        <img src="<?=$root_path?>images/services/1.jpg" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h4 class="title" style="font-weight: bold;">Low Cost <span style="color: #f22613;">Diagnostics</span></h4>
                                    <div class="content">We will provide you 24*7 service and maintenance contract in domestic and commercial sectors with 100% satisfaction guaranteed.</div>
                                    <!-- <a class="button white">Offer Details</a> -->
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-diagnostic.html">
                                        <img src="<?=$root_path?>images/services/2.jpg" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h4 class="title" style="font-weight: bold;">Online <span style="color: #f22613;">Help</span></h4>
                                    
                                    <div class="content">Alexa Taxician Point provide online help for home appliances repair and servicing to all major brands and types
                                    
                                    </div>
                                    <!-- <a class="button white">Offer Details</a> -->
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-upgrades.html">
                                        <img src="<?=$root_path?>images/services/3.jpg" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h4 class="title" style="font-weight: bold;">100 Days <span style="color: #f22613;"> Warranty</span></h4>
                                   
                                    <div class="content">All brand and model Microwave, Refrigerator, Washing machine, Geyser, etc electronic products service available within 24 hours at your doorstep with 100 Days Warranty.
                                    
                                    </div>
                                    <!-- <a class="button white">Offer Details</a> -->
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->
                       </div> <!-- /.three-columns -->
                        
                    </div><!-- /.row -->
                </div><!-- /.container -->   
            </section><!-- /.flat-row -->

            <section class="flat-row parallax parallax1 pad-top70px pad-bottom70px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="title-section style1">
                                <h2 class="title">How it Works</h2>
                                <!-- <p class="desc-title">Do it in just 4 simple steps. A Consumer’s Guide to Automotive Repair in New York.</p> -->
                           </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="flat-divider d40px"></div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="flat-iconbox style1">
                                <div class="icon">
                                    <img src="<?=$root_path?>images/iconbox/icon1.svg" alt="images">
                                </div>
                                <div class="content">
                                    <h4 class="box-title">Best & Transparent Prices</h4>
                                    <!-- <p>Instant Online Quotes from Reputable Garages Close to you</p> -->
                                </div>
                            </div> <!-- /.flat-iconbox -->
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-iconbox style1">
                                <div class="icon">
                                    <img src="<?=$root_path?>images/iconbox/icon2.svg" alt="images">
                                </div>
                                <div class="content">
                                    <h4 class="box-title">On Time Service</h4>
                                    <!-- <p>Get a fixed and fair price estimate for your job</p> -->
                                </div>
                            </div> <!-- /.flat-iconbox -->
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-iconbox style1">
                                <div class="icon">
                                    <img src="<?=$root_path?>images/iconbox/icon1.svg" alt="images">
                                </div>
                                <div class="content">
                                    <h4 class="box-title">Assured Service Quality</h4>
                                    <!-- <p>Choose a certified mechanic and book an appointment</p> -->
                                </div>
                            </div> <!-- /.flat-iconbox -->
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-iconbox style1">
                                <div class="icon">
                                    <img src="<?=$root_path?>images/iconbox/icon2.svg" alt="images">
                                </div>
                                <div class="content">
                                    <h4 class="box-title">Trained Professionals</h4>
                                    <!-- <p>You visit the garage and pay the garage directly for the work</p> -->
                                </div>
                            </div> <!-- /.flat-iconbox -->
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="flat-button" style="text-align: center;">
                                <a class="button outline">Get a free repair quote</a>
                            </div>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <!-- About us -->
            <section class="flat-row popup pad-top70px pad-bottom70px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="title-section">
                                <h2 class="title">About ALEXA TAKNICEN POINT </h2>
                                <!-- <p class="desc-title">Committed to providing the best auto repair services in New York.</p> -->
                           </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="flat-divider d50px"></div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="images-single-flexslider">
                                <ul class="slides">
                                    <li>
                                        <a class="img-post popup-gallery" href="<?=$root_path?>images/images-single/1.jpg"><img src="<?=$root_path?>images/images-single/1.jpg" alt="image"></a>
                                    </li>
                                    <li>
                                        <a class="img-post popup-gallery" href="<?=$root_path?>images/images-single/2.jpg"><img src="<?=$root_path?>images/images-single/2.jpg" alt="image"></a>
                                    </li>
                                </ul>
                            </div><!-- /.images-single-flexslider -->
                        </div><!-- /.col-md-4 -->
                        <div class="modal fade" id="about_show_modal">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Alexa Taxician Point</h4>
                                        <button type="button" class="close" data-dismiss="modal" style="position: relative; top: -0.75em; font-size: 3em;">&times;</button>
                                          
                                    </div>
                                    <div class="modal-body">
                                        <p style="padding: 1em;">We are a leading platform to provide 24*7 on-time and low-cost home appliance repair services. We provide guaranteed and top-quality work and cover all brands with genuine price. For repair of different brands of home appliances there is no need to go to different service centers. Now, you sitting at your house can take service through our company for repair any brand refrigerators, washing machines, microwaves etc home appliances. Our world-class expert engineers have more than 5 years of experience in fixing a wide range of different brand's home appliances. Our verified repair professionals are dedicated to ensuring customers' home appliances are repaired as quickly and efficiently as possible because client satisfaction is our main concern.

                                            Alexa Taxician Point resolve home appliances problems of thousands of customers every week with top-rated professionals in India all cities. Our main motive is to provide one-stop solutions in regard to electrical appliances issues, installation, maintenance, etc. Our professionals are verified and well trained and undergo frequent skill updating programs to keep up them with the technical advancements.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="flat-about-us">
                                <p><span class="dropcap">W</span>e are a leading platform to provide 24*7 on-time and low-cost home appliance repair services. We provide guaranteed and top-quality work and cover all brands with genuine price. For repair of different brands of home appliances there is no need to go to different service centers. Now, you sitting at your house can take service through our company for repair any brand refrigerators, washing machines, microwaves etc home appliances. </p>
                                <a class="button white" data-toggle="modal" data-target="#about_show_modal">Read More</a>
                            </div><!-- /.flat-about-us -->
                        </div><!-- /.col-md-4 -->

                        <div class="col-md-4">
                            <div class="flat-progress-bar">
                                <div class="flat-progress">
                                    <p class="name">Friendly as can be</p>
                                    <div class="progress-bar" data-percent="90" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                                <div class="flat-progress">
                                    <p class="name">Very fair price</p>
                                    <div class="progress-bar" data-percent="80" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                                <div class="flat-progress">
                                    <p class="name">Quick turnaround</p>
                                    <div class="progress-bar" data-percent="70" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                                <div class="flat-progress">
                                    <p class="name">High quality</p>
                                    <div class="progress-bar" data-percent="90" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                            </div><!-- /.flat-progress -->
                        </div><!-- /.col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <section class="flat-row flat-bg-white pad-top70px pad-bottom70px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="title-section">
                                <h2 class="title" style="font-weight: bold;">Our <span style="color: #f22613;">Services</span></h2>
                                <!-- <p class="desc-title">Who is behind the best mechanic service in town?</p> -->
                           </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="member-carousel">
                                <!-- <article class="member">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t8.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">Bryan Hoffman</h3>
                                        <p class="member-subtitle">Co Founder &amp; Mechanic</p>
                                        <ul class="member-meta">
                                            <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t6.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">JAMES WILLIAMS</h3>
                                        <p class="member-subtitle">Co Founder &amp; Mechanic</p>
                                        <ul class="member-meta">
                                            <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t7.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">PATRICK JOHNSON</h3>
                                        <p class="member-subtitle">Mechanic</p>
                                        <ul class="member-meta">
                                            <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article>

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t5.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">NUNO RODRIGUES</h3>
                                        <p class="member-subtitle">Mechanic</p>
                                        <ul class="member-meta">
                                            <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </article> -->

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t4.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">Chimney Repair</h3>
                                        <p class="member-subtitle">Our appliance service center is one of the most leading microwave servicing companies in the INDIA...</p>
                                        <ul class="member-meta">
                                            <!-- <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li> -->
                                        </ul>
                                    </div>
                                </article>

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t3.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">Microwave Oven repair</h3>
                                        <p class="member-subtitle">Our appliance service center is one of the most leading microwave servicing companies in the INDIA...</p>
                                        <ul class="member-meta">
                                            <!-- <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li> -->
                                        </ul>
                                    </div>
                                </article>

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t2.png" alt="t8"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">Refrigerator Repair</h3>
                                        <p class="member-subtitle">In any modern home, fridge is most important appliance and its 24*7 working is of utmost importance.</p>
                                        <ul class="member-meta">
                                            <!-- <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li> -->
                                        </ul>
                                    </div>
                                </article>

                                <article class="member entry object">
                                    <div class="member-image">
                                        <a href="#"><img src="<?=$root_path?>images/member/t1.png" alt="t6"></a>
                                        <div class="member-links">
                                            <div class="social-links">
                                                <a href="#">
                                                    <i class="fa fa-vimeo-square"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-flickr"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-foursquare"></i>
                                                </a>
                                            </div>
                                            
                                            <div class="more-link">
                                                <a href="#">View Profile</a>
                                            </div>
                                        </div>
                                    </div>
                                                
                                    <div class="member-detail">
                                        <h3 class="member-name">Washing Machine Repair</h3>
                                        <p class="member-subtitle">The most common home appliance washing machine repair service any time at your location by our verified technicians.</p>
                                        <ul class="member-meta">
                                            <!-- <li class="member-phone">
                                                <span>Tel:</span>666-888-999                 
                                            </li>
                                            
                                            <li class="member-email">
                                                <a href="#">
                                                    themesflat.com 
                                                </a>
                                            </li> -->
                                        </ul>
                                    </div>
                                </article>
                            </div><!-- /.main-content-wrap -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <section class="flat-row parallax parallax2 pad-top100px pad-bottom100px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="flat-testimonial-owl">
                                <div class="flat-testimonial">
                                    <div class="testimonial-content">
                                        <h3 style="font-weight: bold; color: #fff;">OUR CLIENTS <span style="color:#f22613"> SAY</span></h3>
                                        <img src="<?=$root_path?>images/testimonials/test1.jpg" class="img-fluid" style="border-radius: 50%" />
                                        <p>
                                            "Alexa Taxician Point Home Repair, your maintenance technician was friendly, professional and prompt. Worldclass certified engineers, guaranteed work!"
                                        </p>
                                    </div> 
                                    <div class="testimonial-meta">
                                        <div class="testimonial-image">
                                            <img src="<?=$root_path?>images/testimonials/star.png" alt="images">
                                        </div>
                                        <div class="testimonial-author">
                                            <strong class="author-name">Narendra Pathak</strong>
                                            <div class="author-info">
                                                <span class="subtitle">Businessman</span> 
                                                <!-- <span class="divider">-</span>  -->
                                                <!-- <span class="company">Company LTD</span> -->
                                            </div>
                                        </div>
                                    </div>     
                                </div><!-- /.flat-testimonial -->

                                <div class="flat-testimonial">
                                    <div class="testimonial-content">
                                        <h3 style="font-weight: bold; color: #fff;">OUR CLIENTS <span style="color:#f22613"> SAY</span></h3>
                                        <img src="<?=$root_path?>images/testimonials/test2.jpg" class="img-fluid" style="border-radius: 50%" />
                                        <p>"Very amazing experience with Alexa Taxician Point! Low cost 24*7 home appliance repair service at Alexa Taxician Point!"</p>
                                    </div> 
                                    <div class="testimonial-meta">
                                        <div class="testimonial-image">
                                            <img src="<?=$root_path?>images/testimonials/star.png" alt="images">
                                        </div>
                                        <div class="testimonial-author">
                                            <strong class="author-name">Rohit Singh</strong>
                                            <div class="author-info">
                                                <span class="subtitle">Student</span> 
                                                <!-- <span class="divider">-</span>  -->
                                                <!-- <span class="company">Company LTD</span> -->
                                            </div>
                                        </div>
                                    </div>     
                                </div>

                                <div class="flat-testimonial">
                                    <div class="testimonial-content">
                                        <h3 style="font-weight: bold; color: #fff;">OUR CLIENTS <span style="color:#f22613"> SAY</span></h3>
                                        <img src="<?=$root_path?>images/testimonials/test3.jpg" class="img-fluid" style="border-radius: 50%" />
                                        <p>"Thanks for the ontime service and the fast response. Very satisfied work with genuine price. I can't find a service like you anywhere else. Great Job, Alexa Taxician Point!"</p>
                                    </div> 
                                    <div class="testimonial-meta">
                                        <div class="testimonial-image">
                                            <img src="<?=$root_path?>images/testimonials/star.png" alt="images">
                                        </div>
                                        <div class="testimonial-author">
                                            <strong class="author-name">Pooja Singh</strong>
                                            <div class="author-info">
                                                <span class="subtitle">House Wife</span> 
                                                <!-- <span class="divider">-</span> 
                                                <span class="company">Company LTD</span> -->
                                            </div>
                                        </div>
                                    </div>     
                                </div><!-- /.flat-testimonial -->
                            </div><!-- /.flat-testimonial-owl -->
                        </div><!-- /.col-md-8 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <section class="flat-row flat-bg-white blog blog-grid pad-top70px pad-bottom20px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="title-section">
                                <h2 class="title" style="font-weight: bold;">Recent <span style="color:#f22613;">Work</span></h2>
                                <!-- <p class="desc-title">Be Aware! Keep Your Vehicle Safe, Dependable and on the road longer.</p> -->
                           </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="blog-carousel-slider post-wrap">
                                <article class="entry object">
                                    <div class="feature-post ">
                                        <a class="img-post" href="blog-single.html">
                                            <img src="<?=$root_path?>images/blog/11.jpg" alt="image">
                                        </a>
                                    </div><!-- /.feature-post -->
                                    <!-- <div class="main-post">
                                        <h3 class="entry-title"><a href="blog-single.html">Fresh and Cool This Summer</a></h3>
                                        <div class="entry-meta">
                                            <span class="entry-time">April 24, 2016</span>
                                        </div>
                                        <div class="entry-content">Did you know that manufacturers recommend checking your Climate Control Air Conditioning systems every two years to help keep your...
                                        </div>
                                    </div>/.main-post -->
                                </article><!-- /.entry -->

                                <article class="entry object">
                                    <div class="feature-post ">
                                        <a class="img-post" href="blog-single.html">
                                            <img src="<?=$root_path?>images/blog/1.4.jpg" alt="image">
                                        </a>
                                    </div><!-- /.feature-post -->
                                    <!-- <div class="main-post">
                                        <h3 class="entry-title"><a href="blog-single.html">Steering You Right</a></h3>
                                        <div class="entry-meta">
                                            <span class="entry-time">April 24, 2016</span>
                                        </div>
                                        <div class="entry-content">Most cars these days are fitted with power steering, which essentially means that a pump assists with the steering to make it easi...
                                        </div>
                                    </div>/.main-post -->
                                </article><!-- /.entry -->

                                <article class="entry object">
                                    <div class="feature-post ">
                                        <a class="img-post" href="blog-single.html">
                                            <img src="<?=$root_path?>images/blog/3.jpg" alt="image">
                                        </a>
                                    </div><!-- /.feature-post -->
                                    <!-- <div class="main-post">
                                        <h3 class="entry-title"><a href="blog-single.html">AnyCar is now IT in Envato!</a></h3>
                                        <div class="entry-meta">
                                            <span class="entry-time">April 24, 2016</span>
                                        </div>
                                        <div class="entry-content">Great news for motorists in Milperra, TAG Automotive has joined our ever growing network trusted mechanical workshops. As part of ...
                                        </div>
                                    </div>/.main-post -->
                                </article><!-- /.entry -->

                                <article class="entry object">
                                    <div class="feature-post ">
                                        <a class="img-post" href="blog-single.html">
                                            <img src="<?=$root_path?>images/blog/2.jpg" alt="image">
                                        </a>
                                    </div><!-- /.feature-post -->
                                    <!-- <div class="main-post">
                                        <h3 class="entry-title"><a href="blog-single.html">Get Your Bearings Right</a></h3>
                                        <div class="entry-meta">
                                            <span class="entry-time">April 24, 2016</span>
                                        </div>
                                        <div class="entry-content">If you want to keep your motor running without heat or friction developing you really need to get your bearings right. So, what... 
                                        </div>
                                    </div>/.main-post -->
                                </article><!-- /.entry -->

                                <!-- <article class="entry object">
                                    <div class="feature-post ">
                                        <a class="img-post" href="blog-single.html">
                                            <img src="<?=$root_path?>images/blog/5.jpg" alt="image">
                                        </a>
                                    </div><
                                    <div class="main-post">
                                        <h3 class="entry-title"><a href="blog-single.html">Go With Your Territory?</a></h3>
                                        <div class="entry-meta">
                                            <span class="entry-time">April 24, 2016</span>
                                        </div>
                                        <div class="entry-content">It is fair to say that the Ford Territory and Falcon are two of the most popular vehicles on Australian roads.  Both of these mode...
                                        </div>
                                    </div>
                                </article> -->

                                <!-- <article class="entry object">
                                    <div class="feature-post ">
                                        <a class="img-post" href="blog-single.html">
                                            <img src="<?=$root_path?>images/blog/6.jpg" alt="image">
                                        </a>
                                    </div>
                                    <div class="main-post">
                                        <h3 class="entry-title"><a href="blog-single.html">We’ve Got You Covered</a></h3>
                                        <div class="entry-meta">
                                            <span class="entry-time">April 24, 2016</span>
                                        </div>
                                        <div class="entry-content">We are delighted to welcome three new businesses to our ever growing network of independent mechanical workshops. Wherever you dri...
                                        </div>
                                    </div>
                                </article> -->
                            </div><!-- /.blog-carousel-slider -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->


            <section class="flat-row services pad-bottom0px">
                <div class="container">
                    <h3 class="text-center text-capitalize" style="font-weight: bold;">Our Recent <span style="color: #f22613;"> Blogs</span></h3>
                    
                    <div class="row">
                       <div class="three-columns">
                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-inspection.html">
                                        <img src="<?=$root_path?>images/services/blog1.jpg" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h6 class="title" style="font-weight: bold;">Title :- Long-Lasting Durability </h6>
                                    <p>Admin 15 nov 2018</p>
                                    <h6 class="title" style="font-weight: bold; text-transform: capitalize;">Description</h6>
                                    <div class="content">This home appliance is empowered with features that reduce the formation of dust.</div>
                                    <a href="#" style="font-weight: bold;">Read more ...</a>
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-diagnostic.html">
                                        <img src="<?=$root_path?>images/services/blog2.jpg" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h6 class="title" style="font-weight: bold;">Title :- The Washing Machine Comes Equipped with Special Features</h6>
                                    <p>Admin 15 nov 2018</p>
                                    <h6 class="title" style="font-weight: bold; text-transform: capitalize;">Description</h6>
                                    <div class="content">This type of appliance comes with special features like it has normal, intensive and buzzer.</div>
                                    <a href="#" style="font-weight: bold;">Read more ...</a>
                                    <!-- <a class="button white">Offer Details</a> -->
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-upgrades.html">
                                        <img src="<?=$root_path?>images/services/h2.jpg" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h6 class="title" style="font-weight: bold;">Title :- You can Wash Woolen Clothes Also</h6>
                                    <p>Admin 15 nov 2018</p>
                                    <h6 class="title" style="font-weight: bold; text-transform: capitalize;">Description</h6>
                                    <div class="content">Unlike standard washing machines, Samsung semi-automatic machine is capable to clean blankets and woolen clothes and it dries it rapidly.</div>
                                    <a href="#" style="font-weight: bold;">Read more ...</a>
                                    <!-- <a class="button white">Offer Details</a> -->
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->
                       </div> <!-- /.three-columns -->
                        
                    </div><!-- /.row -->
                </div><!-- /.container -->   
            </section>

            <section class="flat-row home-v1 gallery pad-top0px pad-bottom0px">
                <div class="flat-project flat-animation" data-animation="flipInY" data-animation-delay="0" data-animation-offset="75%">
                    <div class="project-wrap five-columns ">
                        <div class="object project-item entry carwash">
                            <div class="item-wrap ">
                                <div class="project-thumb">
                                    <a href="gallery-grid.html">
                                        <img src="<?=$root_path?>images/gallery/ga1.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="project-info">
                                    <h3 class="project-title">
                                        <a href="gallery-detail.html">Red Car</a>
                                    </h3>
                                    <ul class="project-categories">
                                        <li><a href="gallery-detail.html">Car Wash</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.item-wrap -->
                        </div><!-- /.project-item -->

                        <div class="object project-item  entry carglassing">
                            <div class="item-wrap">
                                <div class="project-thumb">
                                    <a href="gallery-grid.html">
                                        <img src="<?=$root_path?>images/gallery/ga2.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="project-info">
                                    <h3 class="project-title">
                                        <a href="gallery-detail.html">Green Car</a>
                                    </h3>
                                    <ul class="project-categories">
                                        <li><a href="gallery-detail.html">Car Glassing</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.item-wrap -->
                        </div><!-- /.project-item -->

                        <div class="object project-item entry  carpolishing">
                            <div class="item-wrap">
                                <div class="project-thumb">
                                    <a href="gallery-grid.html">
                                        <img src="<?=$root_path?>images/gallery/ga3.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="project-info">
                                    <h3 class="project-title">
                                        <a href="gallery-detail.html">Fire Car</a>
                                    </h3>
                                    <ul class="project-categories">
                                        <li><a href="gallery-detail.html">Car Polishing</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.item-wrap -->
                        </div><!-- /.project-item -->

                        <div class="object project-item  entry  carglassing">
                            <div class="item-wrap">
                                <div class="project-thumb">
                                    <a href="gallery-grid.html">
                                        <img src="<?=$root_path?>images/gallery/ga4.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="project-info">
                                    <h3 class="project-title">
                                        <a href="gallery-detail.html">Cars 2014</a>
                                    </h3>
                                    <ul class="project-categories">
                                        <li><a href="gallery-detail.html">Car Glassing</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.item-wrap -->
                        </div><!-- /.project-item -->

                        <div class="object project-item  entry  carglassing">
                            <div class="item-wrap">
                                <div class="project-thumb">
                                    <a href="gallery-grid.html">
                                        <img src="<?=$root_path?>images/gallery/ga5.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="project-info">
                                    <h3 class="project-title">
                                        <a href="gallery-detail.html">Cars 2014</a>
                                    </h3>
                                    <ul class="project-categories">
                                        <li><a href="gallery-detail.html">Car Glassing</a></li>
                                    </ul>
                                </div>
                            </div><!-- /.item-wrap -->
                        </div><!-- /.project-item -->
                    </div><!-- /.project-wrap -->
                </div><!-- /.flat-project -->
            </section><!-- /.flat-row -->

            <section class="flat-row pad-top0px pad-bottom0px">
                <div class="flat-accordion style1">
                    <div class="flat-toogle">
                        <h3 class="toggle-title active">VIEW US ON THE MAP</h3>
                        <div class="toggle-content">
                            <div id="flat-map"></div>  
                        </div>
                    </div><!-- /.flat-toggle -->
                </div><!-- /.flat-accordion -->
            </section><!-- /.flat-row -->
        </div><!--/.site-content -->

        <?php
            include "footer.php";
        ?>
        
    </div><!-- /.site-wrapper -->    
    
    <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/owl.carousel.js"></script>
    <script type="text/javascript" src="javascript/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="javascript/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIm1AxfRgiI_w36PonGqb_uNNMsVGndKo&v=3.7"></script>
    <script type="text/javascript" src="javascript/gmap3.min.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/imagesloaded.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>

    <!-- Revolution Slider -->
    <script type="text/javascript" src="javascript/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="javascript/slider.js"></script>


</body>
</html>