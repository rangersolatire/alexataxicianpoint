<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>AnyCar - HTML Template for Automotive &amp; Business</title>

    <?php
        include "head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <?php
        include "preloader.php";
    ?>
    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            <?php
                include "header.php";
            ?>

            <!-- Page title -->
            <div class="flat-row page-title  parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="page-title-heading">
                                <h1 class="title">Clients</h1>
                                <p class="subtitle">Alexa Taxician Working Clients</p>
                            </div><!-- /.page-title-captions --> 
                            <div class="breadcrumbs">
                                <p>You are here:</p>
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li>Clients</li>
                                </ul>                   
                            </div><!-- /.breadcrumbs --> 
                        </div><!-- /.col-md-12 -->  
                    </div><!-- /.row -->  
                </div><!-- /.container -->                      
            </div><!-- /.page-title --> 
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <section class="flat-row pad-top0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/5.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/6.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/7.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/8.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->

                    <div class="flat-divider d30px"></div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/9.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/10.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/11.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/12.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->

                    <div class="flat-divider d30px"></div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/5.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/6.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/7.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="flat-clients">
                                <img src="images/client/8.png" alt="images">
                            </div>
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->
        </div><!--/.site-content -->

        <?php
            include "footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>