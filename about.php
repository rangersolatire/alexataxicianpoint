<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>AnyCar - HTML Template for Automotive &amp; Business</title>

    <?php
        include "head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <?php
        include "preloader.php";
    ?>

    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            <?php
                include "header.php";
            ?>

            <!-- Page title -->
            <div class="flat-row page-title  parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="page-title-heading">
                                <h1 class="title">About Us</h1>
                                <p class="subtitle">Having experienced workers and a team of experts to work for you.</p>
                            </div><!-- /.page-title-captions --> 
                            <div class="breadcrumbs">
                                <p>You are here:</p>
                                <ul>
                                    <li><a href="index">Home</a></li>
                                    <li>about us</li>
                                </ul>                   
                            </div><!-- /.breadcrumbs --> 
                        </div><!-- /.col-md-12 -->  
                    </div><!-- /.row -->  
                </div><!-- /.container -->                      
            </div><!-- /.page-title --> 
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <div class="flat-row popup pad-top0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-section">
                                <h2 class="title">Hello, We are Alexa Taknicen Point.</h2>
                                <p class="desc-title">We take care of everything — installation, repairs, changes — all your electronics needs.</p>
                            </div>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="images-single style1">
                                        <div class="thumb">
                                            <a class="popup-gallery" href="images/images-single/1.jpg">
                                                <img src="images/images-single/1.jpg" alt="images">
                                            </a>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="images-single style1">
                                        <div class="thumb">
                                            <a class="popup-gallery" href="images/images-single/2.jpg">
                                                <img src="images/images-single/2.jpg" alt="images">
                                            </a>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.col-md-8 -->

                        <div class="col-md-4">
                            <div class="flat-progress-bar">
                                <div class="flat-progress">
                                    <p class="name">Friendly as can be</p>
                                    <div class="progress-bar" data-percent="90" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                                <div class="flat-progress">
                                    <p class="name">Very fair price</p>
                                    <div class="progress-bar" data-percent="80" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                                <div class="flat-progress">
                                    <p class="name">Quick turnaround</p>
                                    <div class="progress-bar" data-percent="70" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                                <div class="flat-progress">
                                    <p class="name">High quality</p>
                                    <div class="progress-bar" data-percent="90" data-waypoint-active="yes">
                                        <div class="progress-animate"></div>
                                    </div>
                                </div><!-- /.flat-progress -->
                            </div><!-- /.flat-progress -->
                        </div><!-- /.col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.flat-row -->

            <div class="flat-row pad-top0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="flat-tabs">
                                <ul class="menu-tabs clearfix">
                                    <li class="active"><a href="#">History</a></li>
                                    <li><a href="#">Vision</a></li>
                                    <li><a href="#">Our Mission</a></li>
                                </ul>
                                <div class="content-tab">
                                    <div class="content-inner">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidi dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolorem que laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                            </div><!-- /.col-md-8 -->
                                            <div class="col-md-4">
                                                <div class="images-single style1">
                                                    <div class="thumb">
                                                        <a class="popup-gallery" href="images/about/1.jpg">
                                                            <img src="images/about/1.jpg" alt="images">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div><!-- /.col-md-4 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.content-inner -->

                                    <div class="content-inner">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidi dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolorem que laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                            </div><!-- /.col-md-8 -->
                                            <div class="col-md-4">
                                                <div class="images-single style1">
                                                    <div class="thumb">
                                                        <a class="popup-gallery" href="images/about/2.jpg">
                                                            <img src="images/about/2.jpg" alt="images">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div><!-- /.col-md-4 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.content-inner -->

                                    <div class="content-inner">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidi dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exer citation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolorem que laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                            </div><!-- /.col-md-8 -->
                                            <div class="col-md-4">
                                                <div class="images-single style1">
                                                    <div class="thumb">
                                                        <a class="popup-gallery" href="images/about/3.jpg">
                                                            <img src="images/about/3.jpg" alt="images">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div><!-- /.col-md-4 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.content-inner -->
                                </div><!-- /.content-tab -->
                            </div><!-- /.flat-tabs -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.flat-row -->

            <section class="flat-row pad-top0px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="counter">
                                <div class="counter-image">
                                    <img src="images/about/icon5.svg" alt="images">
                                </div>
                                <div class="numb-count" data-to="1280" data-speed="3000" data-waypoint-active="yes">1280</div>
                                <div class="counter-title">
                                    Repairs Solved
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="counter">
                                <div class="counter-image">
                                    <img src="images/about/icon6.svg" alt="images">
                                </div>
                                <div class="numb-count" data-to="4679" data-speed="3000" data-waypoint-active="yes">4679</div>
                                <div class="counter-title">
                                    Happy Customers
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="counter">
                                <div class="counter-image">
                                    <img src="images/about/icon7.svg" alt="images">
                                </div>
                                <div class="numb-count" data-to="3568" data-speed="3000" data-waypoint-active="yes">3568</div>
                                <div class="counter-title">
                                    Premium Customers
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3">
                            <div class="counter">
                                <div class="counter-image">
                                    <img src="images/about/icon8.svg" alt="images">
                                </div>
                                <div class="numb-count" data-to="668" data-speed="3000" data-waypoint-active="yes">668</div>
                                <div class="counter-title">
                                    Clever Workeds
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <section class="flat-row parallax parallax2 pad-top100px pad-bottom100px">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="flat-testimonial-owl">
                                <div class="flat-testimonial">
                                    <div class="testimonial-content">
                                        <p>
                                            You guys do a great job. I have been taking my truck to AnyCar’s for almost three years. I always get great service and the best prices. Keep up the good work.
                                        </p>
                                    </div> 
                                    <div class="testimonial-meta">
                                        <div class="testimonial-image">
                                            <img src="images/testimonials/star.png" alt="images">
                                        </div>
                                        <div class="testimonial-author">
                                            <strong class="author-name">John Doe</strong>
                                            <div class="author-info">
                                                <span class="subtitle">Project Manager</span> 
                                                <span class="divider">-</span> 
                                                <span class="company">Company LTD</span>
                                            </div>
                                        </div>
                                    </div>     
                                </div><!-- /.flat-testimonial -->

                                <div class="flat-testimonial">
                                    <div class="testimonial-content">
                                        <p>
                                            You guys do a great job. I have been taking my truck to AnyCar’s for almost three years. I always get great service and the best prices. Keep up the good work.
                                        </p>
                                    </div> 
                                    <div class="testimonial-meta">
                                        <div class="testimonial-image">
                                            <img src="images/testimonials/star.png" alt="images">
                                        </div>
                                        <div class="testimonial-author">
                                            <strong class="author-name">John Doe</strong>
                                            <div class="author-info">
                                                <span class="subtitle">Project Manager</span> 
                                                <span class="divider">-</span> 
                                                <span class="company">Company LTD</span>
                                            </div>
                                        </div>
                                    </div>     
                                </div><!-- /.flat-testimonial -->
                            </div><!-- /.flat-testimonial-owl -->
                        </div><!-- /.col-md-8 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->

            <section class="flat-row flat-brands">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="brand">
                                <h3 class="title">THE BRANDS WE TRUST.</h3>
                            </div>
                        </div><!-- /.col-md-4 -->
                        <div class="col-md-8">
                            <div class="brands">
                                <div class="brands-featured">
                                    <img src="images/client/1.png" alt="images">
                                </div>
                                <div class="brands-featured">
                                    <img src="images/client/2.png" alt="images">
                                </div>
                                <div class="brands-featured">
                                    <img src="images/client/3.png" alt="images">
                                </div>
                                <div class="brands-featured">
                                    <img src="images/client/4.png" alt="images">
                                </div>
                                <div class="brands-featured">
                                    <img src="images/client/1.png" alt="images">
                                </div>
                            </div><!-- /.brands -->
                        </div><!-- /.col-md-8 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section><!-- /.flat-row -->
        </div><!--/.site-content -->

        <?php
            include "footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/owl.carousel.js"></script>
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery-countTo.js"></script> 
    <script type="text/javascript" src="javascript/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="javascript/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>