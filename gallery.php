<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>AnyCar - HTML Template for Automotive &amp; Business</title>

    <?php
        include "head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <?php
        include "preloader.php";
    ?>
    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            <?php
                include "header.php";
            ?>

            <!-- Page title -->
            <div class="flat-row page-title  parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="page-title-heading">
                                <h1 class="title">Gallery</h1>
                                <p class="subtitle"></p>
                            </div><!-- /.page-title-captions --> 
                            <div class="breadcrumbs">
                                <p>You are here:</p>
                                <ul>
                                    <li><a href="index">Home</a></li>
                                    <li class="active">GALLERY</li>
                                </ul>                   
                            </div><!-- /.breadcrumbs --> 
                        </div><!-- /.col-md-12 -->  
                    </div><!-- /.row -->  
                </div><!-- /.container -->                      
            </div><!-- /.page-title --> 
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <!-- Gallery -->
            <section class="gallery gallery-v3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="flat-project flat-animation" data-animation="flipInY" data-animation-delay="0" data-animation-offset="75%">
                                <div class="controlnav-folio">
                                    <ul class="project-filter">
                                        <li class="active"><a data-filter="*" href="#">All</a></li>
                                        <li><a data-filter=".carwash" href="#">Car Wash</a></li>
                                        <li><a data-filter=".carglassing" href="#">Car Glassing</a></li>
                                        <li><a data-filter=".carpolishing" href="#">Car Polishing</a></li>
                                    </ul><!-- /.project-filter -->
                                </div>
                                <div class="project-wrap three-columns ">
                                    <div class="object project-item entry carwash">
                                        <div class="item-wrap ">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/1.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">Red Car</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/2.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">Green Car</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Glassing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/3.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">Fire Car</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/4.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">Cars 2014</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Glassing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/5.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">Hot Rod</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/6.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">VOLTSWAGON</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->
                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/7.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">CARS 2016</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Glassing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->
                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/8.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">CAR SHOW</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/9.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">BRIDGE</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Glassing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/10.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">BENTLEY</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/11.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">DUESEBERG</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Glassing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/12.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">CITROEN</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/13.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">PORSCHE</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/14.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">RADIATOR</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/15.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">Red Car</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Glassing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/16.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">ROLLS ROYCE</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/17.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">GREEN CAR</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/18.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">BOBBY CAR</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carglassing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/19.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">BLUR CAR</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/20.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">RACE CAR</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/21.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">RACING CAR</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/22.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">WHITE BEETLE</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carpolishing">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/23.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">VW BEETLE</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Polishing</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->

                                    <div class="object project-item  entry  carwash">
                                        <div class="item-wrap">
                                            <div class="project-thumb">
                                                <a href="gallery-grid.html">
                                                    <img src="images/gallery/24.2.jpg" alt="image">
                                                </a>
                                            </div>
                                            <div class="project-info">
                                                <h3 class="project-title">
                                                    <a href="gallery-detail.html">WHITE BEETLE</a>
                                                </h3>
                                                <ul class="project-categories">
                                                    <li><a href="gallery-detail.html">Car Wash</a></li>                                 
                                                </ul>
                                            </div>
                                        </div><!-- /.item-wrap -->
                                    </div><!-- /.project-item -->
                                </div><!-- /.project-wrap -->
                            </div><!-- /.flat-project -->

                        </div>
                    </div>
                </div>
            </section>

            <div class="flat-divider d50px"></div>
        </div><!--/.site-content -->

        <?php
            include "footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/imagesloaded.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>