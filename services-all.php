<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Alexa Taknicen Point | Serices </title>

    <?php
        include "head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <section class="loading-overlay">
        <div class="Loading-Page">
            <h2 class="loader">Loading...</h2>
        </div>
    </section>  
    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            <?php
                include "header.php";
            ?>

            <!-- Page title -->
            <div class="flat-row page-title  parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="page-title-heading">
                                <h1 class="title">SERVICES</h1>
                                <p class="subtitle">We’ve got a wide range of services.</p>
                            </div><!-- /.page-title-captions --> 
                            <div class="breadcrumbs">
                                <p>You are here:</p>
                                <ul>
                                    <li><a href="index">Home</a></li>
                                    <li class="active">SERVICES</li>
                                </ul>                   
                            </div><!-- /.breadcrumbs --> 
                        </div><!-- /.col-md-12 -->  
                    </div><!-- /.row -->  
                </div><!-- /.container -->                      
            </div><!-- /.page-title --> 
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <!-- Services -->
            <section class="services">
                <div class="container">
                    <div class="row">
                       <div class="title-section">
                            <h2 class="title">IT’S PASSION, NOT ONLY WORK!</h2>
                            <p class="desc-title">Using our expertise to create solutions for clients today and in the future</p>
                       </div>
                       <div class="three-columns">
                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="ac-repair-service">
                                        <img src="<?=$root_path?>images/services/ac_repair.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h4 class="title">AC Repair Service</h4>
                                    <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                    </div>
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post" href="services-diagnostic.html">
                                        <img src="<?=$root_path?>images/services/ac_installation.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h4 class="title">AC Installation</h4>
                                    
                                    <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                    
                                    </div>
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->

                            <div class="images-single  object">
                                <div class="thumb">
                                    <a class="img-post" href="services-upgrades.html">
                                        <img src="<?=$root_path?>images/services/ac_dismantel.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                                <div class="desc-img">
                                    <h4 class="title">AC Dismantle</h4>
                                   
                                    <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                    
                                    </div>
                                </div><!-- /.desc-img -->
                            </div><!-- /.images-single -->
                       </div> <!-- /.three-columns -->
                        
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/ac_gas_charge.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">AC Gas Charge</h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/ac_underground_piping.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">AC Underground Piping</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single  object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-upgrades.html">
                                         <img src="<?=$root_path?>images/services/fridge_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Fridge Repair</h4>
                                    
                                     <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/ice_cream_fridge_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Ice-Cream Fridge Repair </h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/washing_machine_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Washing Machine Repair</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single  object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-upgrades.html">
                                         <img src="<?=$root_path?>images/services/semi_washing_machine_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Semi-Automatic Washing Machine Repair</h4>
                                    
                                     <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/washing_machine_installation.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Washine Machine Installation</h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/oven_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Microwave Oven Repair</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single  object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-upgrades.html">
                                         <img src="<?=$root_path?>images/services/ro_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">RO Repair</h4>
                                    
                                     <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/ro_installation.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">RO Installation</h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/plumber_services.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Plumber Service</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single  object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-upgrades.html">
                                         <img src="<?=$root_path?>images/services/drain_cleaning.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Drain Cleaning</h4>
                                    
                                     <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/kitchen_faucet_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Kitchen Faucet Repair</h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/shower_faucet_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Shower Faucet Repair</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single  object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-upgrades.html">
                                         <img src="<?=$root_path?>images/services/gas_bhatti_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Gas Bhatti Repair</h4>
                                    
                                     <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/gas_pipline_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Gas Pipline Repair</h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/geyser_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Geyser Repair</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single  object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-upgrades.html">
                                         <img src="<?=$root_path?>images/services/geyser_installation.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Geyser Installation</h4>
                                    
                                     <div class="content">We take Fridge Repair Services seriously. That’s why we only use the best brands and parts. One of the upgrades that we provide is our Performance…
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                    <div class="row">
                        <div class="three-columns">
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-inspection.html">
                                         <img src="<?=$root_path?>images/services/chimney_repair.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Chimney Repair</h4>
                                     <div class="content">A Anycar Inspection is a comprehensive Ac Repair Service advising the current state of the vehicle. It is a great way to receive independent advice…
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
 
                             <div class="images-single object">
                                 <div class="thumb">
                                     <a class="img-post" href="services-diagnostic.html">
                                         <img src="<?=$root_path?>images/services/welding_works.png" alt="image">
                                     </a>
                                 </div><!-- /.thumb-->
                                 <div class="desc-img">
                                     <h4 class="title">Welding Works</h4>
                                     
                                     <div class="content">We always use the most advanced diagnostic equipment with the most up to date software to make sure that even the most sophisticated car can be….
                                     
                                     </div>
                                 </div><!-- /.desc-img -->
                             </div><!-- /.images-single -->
                        </div> <!-- /.three-columns -->
                    </div>

                </div><!-- /.container -->   
            </section>


            <section class="section-iconbox">
                <div class="container">
                    <div class="row">
                        <div class="title-section">
                            <h2 class="title">WE PROVIDE</h2>
                            <p class="desc-title">Don’t miss out on these limited time savings to keep your car healthy.</p>
                        </div>
                        <div class="two-columns">
                            <div class="flat-iconbox object">
                                <div class="icon">
                                    <i class="fa fa-user-secret"></i>
                                </div>
                                <div class="content">
                                    <h4>EXPERT TECHNICIANS</h4>
                                    <p>
                                        How are you supposed to know which ones will be honest, experienced, efficient, and affordable? We want you to know that you are in good hands when you have an Expert technician to your home.
                                    </p>
                                </div>
                            </div> <!-- /.flat-iconbox -->

                            <div class="flat-iconbox object">
                                <div class="icon">
                                    <i class="fa fa-diamond"></i>
                                </div>
                                <div class="content">
                                    <h5>EXCELLENT REVIEWS FROM CUSTOMERS</h5>
                                    <p>
                                        A good review includes enough detail to give others a feel for what happened. Explain which factors contributed to your positive, negative or just so-so experience.
                                    </p>
                                </div>
                            </div> <!-- /.flat-iconbox -->
                        </div> <!-- /.two-columns -->                        
                    </div><!-- /.row -->
                </div><!-- /.container -->   
            </section>

            <section class="flat-row contact-now parallax parallax2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>SAVE YOUR MONEY</h3>
                            <h2>WITH <strong>DISCOUNT SPECIALS </strong> AND AUTO SERVICE <strong> COUPONS</strong></h2>
                            <a href="contact-us" class="button outline">Contact Now!</a>
                        </div>
                    </div>
                </div>
            </section>
        </div><!--/.site-content -->

        <?php
            include "footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>