<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>AC Repair Service | Alexa Taxician Point</title>

    <?php
        include "../head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <?php
        include "../preloader.php";
    ?>

    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            <?php
                include "../header.php";
            ?>

            <!-- Page title -->
            <div class="flat-row page-title  parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="page-title-heading">
                                <h1 class="title">AC REPAIR SERVICES </h1>
                                <p class="subtitle">Using our expertise to get solutions for your AC today and in the future</p>
                            </div><!-- /.page-title-captions --> 
                            <div class="breadcrumbs">
                                <p>You are here:</p>
                                <ul>
                                    <li><a href="index">Home</a></li>
                                    <li><a href="services-all">SERVICES</a></li>
                                    <li class="active">AC REPAIR SERVICES</li>
                                </ul>                   
                            </div><!-- /.breadcrumbs --> 
                        </div><!-- /.col-md-12 -->  
                    </div><!-- /.row -->  
                </div><!-- /.container -->                      
            </div><!-- /.page-title --> 
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <!-- Services -->
            <section class="services popup">
                <div class="container">
                    <div class="row">
                       <div class="three-columns">
                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post popup-gallery" href="<?=$root_path?>images/services/1.jpg">
                                        <img src="<?=$root_path?>images/services/ac_repair.png" alt="image">
                                    </a>
                                </div>
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post popup-gallery" href="<?=$root_path?>images/services/2.jpg">
                                        <img src="<?=$root_path?>images/services/ac_installation.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                            </div><!-- /.images-single -->

                            <div class="images-single  object">
                                <div class="thumb">
                                    <a class="img-post popup-gallery" href="<?=$root_path?>images/services/3.jpg">
                                        <img src="<?=$root_path?>images/services/ac_dismantel.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                            </div><!-- /.images-single -->
                       </div> <!-- /.three-columns -->

                       <div class="two-columns">
                            <div class="flat-text-content object">
                                <h3 class="title">OVERVIEW</h3>
                                <div class="content">
                                    <p>The Uttarakhand state experiences brutal summer months. Consider this, what if you are residing in this locality and your air-conditioner, suddenly stops working? Well, your air-conditioners (AC) need maintenance. We, at Alexa Taxician Point are the leading service provider of ac repair in your area. </p>

                                    <p>AC repair services are available at an affordable cost. The company offers a comprehensive range of air-conditioner (AC) repair. Any kind of repair work for air-conditioner is undertaken by Alexa Taknicen Point. </p>
                                    
                                    <p>We offer air-conditioner gas refilling, advanced piping or servicing also split air-conditioner installation and services, etc. at your doorstep.</p>
                                    
                                    <p>All you need to do is to log on to our website Alexa Taknicen Point and click on air-conditioner (AC) repair services. You can select the professional depending upon your choice.</p>
                                    
                                    <p>For ac repair, the qualified technician will come to your home or office and check the problem and how it can be resolved. The price will be quoted by our technician. Once approved, our technician will explain the root cause of the problem.</p>
                                    
                                    <p>If the issue is small, AC repair will be done at home itself. Otherwise, it will be taken to the service station for the ac repair. You can call us now or you can book ac repair support online.</p>
                                </div>
                            </div><!-- /.text-content -->

                            <div class="flat-text-content object">
                                <h3 class="title">All Brands AC Repair Service Center</h3>
                                <div class="content">
                                    <p>Ac Repair At Clock Tower, Ac Repair At Rajpur Road, Ac Repair At Gandhi Park, Ac Repair At Parade Ground, Ac Repair At Dilaram Chowk, Ac Repair At mussoorie diversion, Ac Repair At Canal Road, Ac Repair At Kuthal Gate, Ac Repair At Jakhan, Ac Repair At Sahastradhara Road, Ac Repair At Sahastradhara Crossing, Ac Repair At DL Road, Ac Repair At Mayur Chowki, Ac Repair At Mandakini Vihar, Ac Repair At IT Park, Ac Repair At Gujrara Mansingh, Ac Repair At Gol Chakkar, Ac Repair At Helipad, Ac Repair At Pacific Golf Estate, Ac Repair At Sahastradhara, Ac Repair At Raipur Road, Ac Repair At 6 No Puliya, Ac Repair At Balawala, Ac Repair At Miya Wala, Ac Repair At Haridwar Road, Ac Repair At Rispina Bridge, Ac Repair At Ec Road, Ac Repair At Bhagat Singh Chowk, Ac Repair At Raja Road, Ac Repair At Building, Ac Repair At Paltan Bazar, Ac Repair At Chat Wali Gali, Ac Repair At Bindal Bridge, Ac Repair At Doon College, Ac Repair At Gadi Cannt, Ac Repair At Ballupur Chowk, Ac Repair At Premnagar, Ac Repair At Nanda Ki Chowki, Ac Repair At Law College, Ac Repair At ISBT, Ac Repair At Graphic Era School, Ac Repair At Sabji Mandi, Ac Repair At Saharanpur Chowk, Ac Repair At Jhanda Chowk, Ac Repair At GandhiGram, Ac Repair At Khurbura Mohalla, Ac Repair At Balliwala Chowk, Ac Repair At GMS Road, Ac Repair At Indira Nagar, Ac Repair At Vikas Mall, Ac Repair At Malik Chowk, Ac Repair At ISBT Gate, Ac Repair At JogiWala, Ac Repair At Vikas Nagar Road</p>
                                </div>
                            </div><!-- /.text-content -->
                       </div>
                        
                    </div><!-- /.row -->
                </div><!-- /.container -->   
            </section>

            <!-- <section class="section-download">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flat-download">
                                <div class="icon">
                                    <i class="fa fa-download"></i>
                                </div>
                                <div class="content">
                                    <div class="text-content">
                                        <h2>Brochure</h2>
                                        <p>I am promo text. Click button to Download Brochure.</p>
                                    </div>
                                    <a href="#" class="button">Download</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="flat-download">
                                <div class="icon">
                                    <i class="fa fa-dollar"></i>
                                </div>
                                <div class="content">
                                    <div class="text-content">
                                        <h2>Pricing</h2>
                                        <p>I am promo text. Click button to Download Pricing Table.</p>
                                    </div>
                                    <a href="#" class="button">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->

            <div class="flat-divider d20px"></div>
        </div><!--/.site-content -->

        <?php
            include "../footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="../javascript/jquery.min.js"></script>
    <script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="../javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="../javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="../javascript/parallax.js"></script>
    <script type="text/javascript" src="../javascript/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="../javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="../javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="../javascript/main.js"></script>

</body>
</html>