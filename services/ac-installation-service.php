<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>AC Installation Service | Alexa Taxician Point</title>

    <?php
        include "../head.php";
    ?>
</head>                                 
<body class="header-sticky">
    <?php
        include "../preloader.php";
    ?>

    <div id="site-wrapper">

        <!-- Site-header -->
        <div id="site-header">
            <?php
                include "../header.php";
            ?>

            <!-- Page title -->
            <div class="flat-row page-title  parallax parallax1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">                    
                            <div class="page-title-heading">
                                <h1 class="title">AC INSTALLATION SERVICES </h1>
                                <p class="subtitle">Using our expertise to get solutions for your AC today and in the future</p>
                            </div><!-- /.page-title-captions --> 
                            <div class="breadcrumbs">
                                <p>You are here:</p>
                                <ul>
                                    <li><a href="<?=$root_path?>index">Home</a></li>
                                    <li><a href="<?=$root_path?>services-all">SERVICES</a></li>
                                    <li class="active">AC INSTALLATION SERVICES</li>
                                </ul>                   
                            </div><!-- /.breadcrumbs --> 
                        </div><!-- /.col-md-12 -->  
                    </div><!-- /.row -->  
                </div><!-- /.container -->                      
            </div><!-- /.page-title --> 
        </div><!--  /.site-header -->
        
        <div id="site-content">
            <!-- Services -->
            <section class="services popup">
                <div class="container">
                    <div class="row">
                       <div class="three-columns">
                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post popup-gallery" href="<?=$root_path?>images/services/1.jpg">
                                        <img src="<?=$root_path?>images/services/ac_Installation.png" alt="image">
                                    </a>
                                </div>
                            </div><!-- /.images-single -->

                            <div class="images-single object">
                                <div class="thumb">
                                    <a class="img-post popup-gallery" href="<?=$root_path?>images/services/2.jpg">
                                        <img src="<?=$root_path?>images/services/ac_installation.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                            </div><!-- /.images-single -->

                            <div class="images-single  object">
                                <div class="thumb">
                                    <a class="img-post popup-gallery" href="<?=$root_path?>images/services/3.jpg">
                                        <img src="<?=$root_path?>images/services/ac_dismantel.png" alt="image">
                                    </a>
                                </div><!-- /.thumb-->
                            </div><!-- /.images-single -->
                       </div> <!-- /.three-columns -->

                       <div class="two-columns">
                            <div class="flat-text-content object">
                                <h3 class="title">OVERVIEW</h3>
                                <div class="content">
                                    <p>The Uttarakhand state experiences brutal summer months. Consider this, what if you are residing in this locality and your air-conditioner, suddenly stops working? Well, your air-conditioners (AC) need maintenance. We, at Alexa Taxician Point are the leading service provider of ac installation in your area. </p>

                                    <p>AC installation services are available at an affordable cost. The company offers a comprehensive range of air-conditioner (AC) installation. Any kind of installation work for air-conditioner is undertaken by Alexa Taknicen Point. </p>
                                    
                                    <p>We offer air-conditioner gas refilling, advanced piping or servicing also split air-conditioner installation and services, etc. at your doorstep.</p>
                                    
                                    <p>All you need to do is to log on to our website Alexa Taknicen Point and click on services (AC) installation services. You can select the professional depending upon your choice.</p>
                                    
                                    <p>For ac installation, the qualified technician will come to your home or office. The price will be quoted by our technician. Our technician will explain the process and will setup the installation.</p>
                                </div>
                            </div><!-- /.text-content -->

                            <div class="flat-text-content object">
                                <h3 class="title">All Brands AC Installation Service Center</h3>
                                <div class="content">
                                    <p>Ac Installation At Clock Tower, Ac Installation At Rajpur Road, Ac Installation At Gandhi Park, Ac Installation At Parade Ground, Ac Installation At Dilaram Chowk, Ac Installation At mussoorie diversion, Ac Installation At Canal Road, Ac Installation At Kuthal Gate, Ac Installation At Jakhan, Ac Installation At Sahastradhara Road, Ac Installation At Sahastradhara Crossing, Ac Installation At DL Road, Ac Installation At Mayur Chowki, Ac Installation At Mandakini Vihar, Ac Installation At IT Park, Ac Installation At Gujrara Mansingh, Ac Installation At Gol Chakkar, Ac Installation At Helipad, Ac Installation At Pacific Golf Estate, Ac Installation At Sahastradhara, Ac Installation At Raipur Road, Ac Installation At 6 No Puliya, Ac Installation At Balawala, Ac Installation At Miya Wala, Ac Installation At Haridwar Road, Ac Installation At Rispina Bridge, Ac Installation At Ec Road, Ac Installation At Bhagat Singh Chowk, Ac Installation At Raja Road, Ac Installation At Building, Ac Installation At Paltan Bazar, Ac Installation At Chat Wali Gali, Ac Installation At Bindal Bridge, Ac Installation At Doon College, Ac Installation At Gadi Cannt, Ac Installation At Ballupur Chowk, Ac Installation At Premnagar, Ac Installation At Nanda Ki Chowki, Ac Installation At Law College, Ac Installation At ISBT, Ac Installation At Graphic Era School, Ac Installation At Sabji Mandi, Ac Installation At Saharanpur Chowk, Ac Installation At Jhanda Chowk, Ac Installation At GandhiGram, Ac Installation At Khurbura Mohalla, Ac Installation At Balliwala Chowk, Ac Installation At GMS Road, Ac Installation At Indira Nagar, Ac Installation At Vikas Mall, Ac Installation At Malik Chowk, Ac Installation At ISBT Gate, Ac Installation At JogiWala, Ac Installation At Vikas Nagar Road</p>
                                </div>
                            </div><!-- /.text-content -->
                       </div>
                        
                    </div><!-- /.row -->
                </div><!-- /.container -->   
            </section>

            <!-- <section class="section-download">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flat-download">
                                <div class="icon">
                                    <i class="fa fa-download"></i>
                                </div>
                                <div class="content">
                                    <div class="text-content">
                                        <h2>Brochure</h2>
                                        <p>I am promo text. Click button to Download Brochure.</p>
                                    </div>
                                    <a href="#" class="button">Download</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="flat-download">
                                <div class="icon">
                                    <i class="fa fa-dollar"></i>
                                </div>
                                <div class="content">
                                    <div class="text-content">
                                        <h2>Pricing</h2>
                                        <p>I am promo text. Click button to Download Pricing Table.</p>
                                    </div>
                                    <a href="#" class="button">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->

            <div class="flat-divider d20px"></div>
        </div><!--/.site-content -->

        <?php
            include "../footer.php";
        ?>
        
    </div>  <!-- /.site-wrapper -->
   
    <!-- Javascript -->
    <script type="text/javascript" src="../javascript/jquery.min.js"></script>
    <script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="../javascript/jquery.sticky.js"></script>
    <script type="text/javascript" src="../javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="../javascript/parallax.js"></script>
    <script type="text/javascript" src="../javascript/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="../javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="../javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="../javascript/main.js"></script>

</body>
</html>