<div id="headerbar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom-info">
                                <i class="fa fa-clock-o">
                                </i>We'are Open: Monday - Saturday, 8:00 Am - 18:00 Pm
                            </div><!-- /.custom-info -->

                            <div class="social-links">
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-behance"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-spotify"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-rss"></i>
                                </a>
                            </div><!-- /.social-links -->

                            <!-- <nav id="site-navigator" class="top-navigator">
                                <ul id="menu-top-menu" class="menu">
                                    <li class="menu-item-has-children"><a href="#">Extra Pages</a>
                                        <ul class="sub-menu">
                                            <li ><a href="maintenance-mode.html">Maintenance Mode</a></li>
                                            <li ><a href="coming-soon.html">Coming Soon</a></li>
                                            <li><a href="404.html">404 Page</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Elements</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Checkout</a></li>
                                </ul>
                            </nav> -->
                            <!-- /.top-navigator -->
                        </div>
                    </div>
                </div><!-- /.container -->
            </div><!-- /.headerbar -->

            <!-- Header -->            
            <header id="header" class="header clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="header-wrap clearfix">
                                <div id="logo" class="logo">
                                    <a href="index.html" rel="home">
                                        <img src="<?=$root_path?>images/logo.png" alt="image">
                                    </a>
                                </div><!-- /.logo -->
                                <div class="nav-wrap">
                                    <div class="btn-menu"></div><!-- //mobile menu button -->
                                    <nav id="mainnav" class="mainnav">
                                        <ul class="menu"> 
                                            <li class="home">
                                                <a href="index">Home</a>
                                            </li>
                                            <li><a href="about">About</a>
                                                <ul class="submenu"> 
                                                    <li><a href="about">About Us</a></li>
                                                    <li><a href="about-client">Clients</a></li>
                                                    <li><a href="about-reviews.html">Reviews</a></li>
                                                </ul><!-- /.submenu -->
                                            </li>

                                            <li><a href="services-all">Services</a>
                                                <ul class="submenu"> 
                                                    <li><a href="<?=$root_path?>services-all">All Services</a></li>
                                                    <li><a href="<?=$root_path?>services/ac-repair-service">AC Repair Service</a></li>
                                                    <li><a href="<?=$root_path?>services/ac-installation-service">AC Installation Service</a></li>
                                                    <li><a href="services-upgrades.html">AC Dismantle Serice</a></li>
                                                    <li><a href="services-pricing.html">AC Gas Charge</a></li>
                                                    <li><a href="services-detail.html">AC Underground Piping</a></li>
                                                    <li><a href="services-detail.html">Fridge Repair</a></li>
                                                    <li><a href="services-detail.html">Ice-Cream Fridge Repair</a></li>
                                                    <li><a href="services-detail.html">Washing Machine Repair</a></li>
                                                    <li><a href="services-detail.html">Semi Automatic Washing Machine Repair</a></li>
                                                    <li><a href="services-detail.html">Washing Machine Installation</a></li>
                                                    <li><a href="services-detail.html">Microwave Oven Repair</a></li>
                                                    <li><a href="services-detail.html">RO Repair</a></li>
                                                    <li><a href="services-detail.html">RO Installation</a></li>
                                                    <li><a href="services-detail.html">Plumber Service</a></li>
                                                    <li><a href="services-detail.html">Drain Cleaning</a></li>
                                                    <li><a href="services-detail.html">Kitchen Faucet Repair</a></li>
                                                    <li><a href="services-detail.html">Shower Faucet Repair</a></li>
                                                    <li><a href="services-detail.html">Gas Bhatti Repair</a></li>
                                                    <li><a href="services-detail.html">Gas Pipline Repair</a></li>
                                                    <li><a href="services-detail.html">Geyser Repair</a></li>
                                                    <li><a href="services-detail.html">Geyser Installation</a></li>
                                                    <li><a href="services-detail.html">Welding Works</a></li>
                                                    <li><a href="services-detail.html">Fabrication Works</a></li>
                                                    <li><a href="services-detail.html">Metal Cutting</a></li>
                                                    <li><a href="services-detail.html">CNC Designing</a></li>
                                                </ul><!-- /.submenu -->
                                            </li>

                                            <li><a href="blog-default.html">Blog</a>
                                                <ul class="submenu"> 
                                                    <li><a href="blog-default.html">Blog Default</a></li>
                                                    <li><a href="blog-grid.html">Blog Grid</a></li>
                                                    <li><a href="blog-medium.html">Blog Medium</a></li>
                                                </ul><!-- /.submenu -->
                                            </li>                 
                                            <li><a href="gallery">Gallery</a></li>
                                            <li><a href="contact-us">Contact</a></li>
                                        </ul><!-- /.menu -->
                                    </nav><!-- /.mainnav -->
                                </div><!-- /.nav-wrap -->
                                
                            </div><!-- /.header-wrap -->
                        </div>
                    </div> <!-- /.row -->
                </div> <!--  /.container -->
            </header><!-- /.header -->