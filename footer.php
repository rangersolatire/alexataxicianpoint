<div class="site-footer">
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">                            
                        <div id="text01" class="widget widget_text ">
                            <div class="textwidget">
                                <div class="footer-info">
                                    <p>
                                        <img  alt="AnyCar" src="<?=$root_path?>images/logo1.png">
                                    </p>
                                    <p>We offer a commitment to personalized service for our clients. If you have further questions or need help with a case, please complete our quick form below. A team<br>member will return your message as soon as possible.</p>
                                    <div class="two-columns row">
                                        <div class="object">
                                            <i class="fa fa-map-marker"></i>
                                            <strong>Raja Ram Vihar,IT Park, Sahastradhara Road, Dehradun, Uttarakhand 248001</strong> <br>
                                            <i class="fa fa-phone"></i>
                                            <strong>Tel:</strong> +91-7060767271
                                        </div>
                                        <div class="object ft-phone">
                                            <i class="fa fa-tablet"></i>
                                            <strong>Mobile: </strong>+91-7060767271<br>
                                            <i class="fa fa-envelope"></i>
                                            <strong>E-mail:</strong>
                                            <a href="#">alexataknicenpoint@gmail.com</a>
                                        </div>
                                    </div><!-- /.row -->
                                    <a href="#" class="button">
                                        <i class="fa fa-download"></i>Download Brochure</a>

                                </div>
                            </div>
                        </div>
                        <div class="widget widget_nav_menu">
                            <h3 class="widget-title">Useful Links</h3>
                            <div class="menu-sample-pages-container">
                                <ul  class="menu">
                                    <li><a href="about">About Us</a></li>
                                    <li><a href="services-all">Services</a></li>
                                    <li><a href="gallery">Gallery</a></li>
                                    <li><a href="contact-us">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                   
                        <div class="widget widget_nav_menu">
                            <h3 class="widget-title">Services</h3>
                            <div class="menu-sample-pages-container">
                                <ul  class="menu">
                                    <li ><a href="ac-repair-service">AC Repair</a></li>
                                    <li ><a href="oven-repair">Microwave Oven repair</a></li>
                                    <li ><a href="fridge-repair">Refrigerator Repair</a></li>
                                    <li ><a href="washing-machine-repair">Washing Machine Repair</a></li>
                                    <li ><a href="chimney-repair">Chimney Repair</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    
                        <div class="widget widget_nav_menu">
                            <h3 class="widget-title">Opening Hours</h3>
                            <div class="menu-sample-pages-container">
                                <ul  class="menu">
                                    <li ><a href="#">Mon - 08:00am - 10:00pm</a></li>
                                    <li ><a href="#">Tue - 08:00am - 10:00pm</a></li>
                                    <li ><a href="#">Wed - 08:00am - 10:00pm</a></li>
                                    <li ><a href="#">Thu - 08:00am - 10:00pm</a></li>
                                    <li ><a href="#">Fri - 08:00am - 10:00pm</a></li>
                                    <li ><a href="#">Sat - 08:00am - 10:00pm</a></li>
                                    <li ><a href="#">Sun - Closed</a></li>
                                </ul>
                            </div>
                        </div>                        
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-widgets -->
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <div class="social-links">
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-behance"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-spotify"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fa fa-rss"></i>
                                    </a>
                                </div><!-- /.social-links -->
                                <div class="copyright-content">
                                    Copyright © 2021 Alexa Taxician Point. Designed and Developed by <a href="https://www.linkedin.com/in/parth22/">Parth Singh.</a>
                                </div><!-- /.copyright-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Go Top -->
            <a class="go-top">
            </a> 
        </div>